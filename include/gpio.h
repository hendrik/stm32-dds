#ifndef _GPIO_H
#define _GPIO_H

#include "util.h"

#include <stm32f4xx_gpio.h>

typedef struct
{
  GPIO_TypeDef* group;
  unsigned int pin;
} gpio_pin;

/* definition of all used GPIO ports */
#define DEF_GPIO(name, _group, _pin)                                           \
  static const gpio_pin name = {.group = GPIO##_group, .pin = _pin }

DEF_GPIO(ETHERNET_RESET, A, 6);

DEF_GPIO(IO_UPDATE, D, 11);
DEF_GPIO(IO_RESET, B, 7);

DEF_GPIO(RF_SWITCH, A, 4);
DEF_GPIO(TRIGGER_SELECT, C, 0);
DEF_GPIO(EXTERNAL_TRIGGER, B, 1);
DEF_GPIO(PLL_LOCK, C, 15);
DEF_GPIO(DDS_RESET, A, 0);

DEF_GPIO(PROFILE_0, D, 10);
DEF_GPIO(PROFILE_1, D, 9);
DEF_GPIO(PROFILE_2, D, 8);

DEF_GPIO(LED_RED, D, 14);
DEF_GPIO(LED_ORANGE, D, 13);
DEF_GPIO(LED_BLUE, D, 15);
DEF_GPIO(LED_FRONT, B, 9);

DEF_GPIO(RED_BUTTON, D, 12);

DEF_GPIO(DRCTL, D, 2);
DEF_GPIO(DRHOLD, D, 3);
DEF_GPIO(DROVER, D, 1);

DEF_GPIO(TX_ENABLE, B, 0);
DEF_GPIO(PARALLEL_F0, B, 15);
DEF_GPIO(PARALLEL_F1, B, 14);

DEF_GPIO(PARALLEL_D0, E, 0);
DEF_GPIO(PARALLEL_D1, E, 1);
DEF_GPIO(PARALLEL_D2, E, 2);
DEF_GPIO(PARALLEL_D3, E, 3);
DEF_GPIO(PARALLEL_D4, E, 4);
DEF_GPIO(PARALLEL_D5, E, 5);
DEF_GPIO(PARALLEL_D6, E, 6);
DEF_GPIO(PARALLEL_D7, E, 7);
DEF_GPIO(PARALLEL_D8, E, 8);
DEF_GPIO(PARALLEL_D9, E, 9);
DEF_GPIO(PARALLEL_D10, E, 10);
DEF_GPIO(PARALLEL_D11, E, 11);
DEF_GPIO(PARALLEL_D12, E, 12);
DEF_GPIO(PARALLEL_D13, E, 13);
DEF_GPIO(PARALLEL_D14, E, 14);
DEF_GPIO(PARALLEL_D15, E, 15);

#undef DEF_GPIO

/* basic GPIO functions defined as inline to save call time */
static INLINE void gpio_set_high(gpio_pin);
static INLINE void gpio_set_low(gpio_pin);
static INLINE void gpio_set(gpio_pin, int);
static INLINE void gpio_toggle(gpio_pin);
static INLINE void gpio_set_port(GPIO_TypeDef* GPIOx, uint16_t value);
static INLINE int gpio_get_input(gpio_pin);
static INLINE int gpio_get_output(gpio_pin);

void gpio_init(void);

void gpio_set_pin_mode_input(gpio_pin);
void gpio_set_pin_mode_output(gpio_pin);

void gpio_blink_forever_slow(gpio_pin);
void gpio_blink_forever_fast(gpio_pin);

/** implementation starts here */

static INLINE void
gpio_set_high(gpio_pin pin)
{
  /* directly write to the correct register. The function from ST do a
   * bunch of sanity checks which slow things down a lot. We want maximum
   * speed here */
  pin.group->BSRRL = 1 << pin.pin;
}

static INLINE void
gpio_set_low(gpio_pin pin)
{
  /* directly write to the correct register. The function from ST do a
   * bunch of sanity checks which slow things down a lot. We want maximum
   * speed here */
  pin.group->BSRRH = 1 << pin.pin;
}

static INLINE void
gpio_toggle(gpio_pin pin)
{
  pin.group->ODR ^= 1 << pin.pin;
}

static INLINE void
gpio_set(gpio_pin pin, int value)
{
  if (value) {
    gpio_set_high(pin);
  } else {
    gpio_set_low(pin);
  }
}

static INLINE void
gpio_set_port(GPIO_TypeDef* GPIOx, uint16_t value)
{
  GPIOx->ODR = value;
}

/* get the current status of an input pin. This does not work for pins
 * configured in the output mode */
static INLINE int
gpio_get_input(gpio_pin pin)
{
  /* directly read from the correct register. The ST functions do a bunch
   * of sanity checks which slow things down a lot. We want maximum speed
   * here. We use !! (not not) to make sure that the output is 0 or 1 */
  return !!(pin.group->IDR & (1 << pin.pin));
}

static INLINE int
gpio_get_output(gpio_pin pin)
{
  /* directly read from the correct register. The ST functions do a bunch
   * of sanity checks which slow things down a lot. We want maximum speed
   * here. We use !! (not not) to make sure that the output is 0 or 1 */
  return !!(pin.group->ODR & (1 << pin.pin));
}

#endif /* _GPIO_H */
