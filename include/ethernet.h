#ifndef __ETHERNET_H
#define __ETHERNET_H

#include "data.h"

#include <lwip/err.h>
#include <stddef.h>
#include <stdint.h>

void ethernet_init(void);
void ethernet_loop(void);

/**
 * queue data for transmision to the client. If you can guarantee that the
 * data will be valid until end of send (e.g. forever) use queue,
 * otherwise use copy_queue. */
err_t ethernet_queue(const char*, uint16_t length);
err_t ethernet_copy_queue(const char*, uint16_t length);

/**
 * store the next len bytes directly into dest without parsing.
 * offset specifies the number of bytes to ignore from the current parser
 * packet. This is used to read parallel data into memory
 */
size_t ethernet_copy_data(void* dest, size_t len, size_t offset);
/**
 * similar to ethernet_copy_data this function instead sends the data via
 * SPI to the DDS.
 */
size_t ethernet_send_data(size_t len, size_t offset);

#endif /* __ETHERNET_H */
