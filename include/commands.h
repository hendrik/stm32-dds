#ifndef _COMMANDS_H
#define _COMMANDS_H

#include "ad9910.h"
#include "gpio.h"

#include <stddef.h>
#include <stdint.h>

/* how many bytes are reserved to queue commands. */
#define COMMAND_QUEUE_LENGTH 2048

/* a flag signalizing whether we are executing or not */
extern uint8_t command_execute_flag;

/* a macro definition of all command types. A command type and some
 * functions for queuing and exeuting are generated from this macro.
 *
 * To add a new command add it to the list and add the matching struct
 * (starting with command_) later in this file. Make sure you've read and
 * understood the notes on the generic command struct.
 */
#define COMMANDS_LIST(F)                                                       \
  F(end)                                                                       \
  F(register)                                                                  \
  F(pin)                                                                       \
  F(trigger)                                                                   \
  F(wait)                                                                      \
  F(update)                                                                    \
  F(spi_write)                                                                 \
  F(frequency)                                                                 \
  F(amplitude)                                                                 \
  F(phase)                                                                     \
  F(parallel)                                                                  \
  F(parallel_frequency)                                                        \
  F(ram_state)                                                                 \
  F(ram_destination)                                                           \
  F(ram_program)                                                               \
  F(ram_control)                                                               \
  F(ramp_state)                                                                \
  F(ramp_destination)                                                          \
  F(ramp_direction_toggle)

/* define an enum with values for each command type */
#define COMMAND_TYPE_ENUM_ENTRY(cmd) command_type_##cmd,

typedef enum { COMMANDS_LIST(COMMAND_TYPE_ENUM_ENTRY) } command_type;

/**
 * Generic command struct which should never be used to queue commands. It
 * is used as a generic command. You can read a command as generic
 * command, then read the type and cast it to the correct type.
 *
 * NOTE: All structs must have the same elements in the same order as this
 * struct on their beginning. Otherwise casting will give wrong results
 * and the program will crash. If you don't need any extra fields you can
 * just add a typedef from this command to the new command type.
 *
 * NOTE: All command structs require the ALIGNED definition. This macro
 * expands to a processor directive which ensures data algnment with the
 * memory boundaries. This ensures that everything is aligned even if we
 * build a list of commands. If commands are not aligned the processor
 * will issue a fault and abort the program execution (there is no special
 * fault handler for this case so you will end up in the
 * HardFailt_Handler)..
 */
typedef struct
{
  command_type type;
} ALIGNED command;

/**
 * special command signalizing the end of the command sequence.
 */
typedef command command_end;

/**
 * change a given register value to the new value. Is the command
 * equivalent of the ad9910_set_value function.
 *
 * To reduce writes to the processor the changes are not writen directly
 * to the DDS, but queued until a spi_write command is issued. This
 * ensures that registers are written once even if multiple bits change
 *
 * Changes will get active in the DDS only after an IO update has been
 * issued (either by sending the io_update command or by an external
 * trigger.
 */
typedef struct
{
  command_type type;
  const ad9910_register_bit* reg;
  uint32_t value;
} ALIGNED command_register;

/**
 * change the state of an output pin to the given value.
 *
 * Unlike register changes these happen immediatly and are thus don't wait
 * for the next trigger. If you want to trigger pin changes first queue
 * the trigger_wait and then the pin command. This executes the pin
 * command as soon as the trigger is over.
 */
typedef struct
{
  command_type type;
  const gpio_pin pin;
  int value;
} ALIGNED command_pin;

/**
 * Wait for an external trigger. This command acts as some sort of barrier
 * which groups changes together. Notable exception are the pin commannds
 * (see above)
 */
typedef command command_trigger;

/**
 * wait for a given period of time (in milliseconds)
 */
typedef struct
{
  command_type type;
  uint32_t delay;
} ALIGNED command_wait;

/**
 * send an io update to the DDS. This has limited use in programming a
 * sequence. It is however essential to perform single updates right awaiy
 * (i.e. in normal mode).
 */
typedef command command_update;

/**
 * write the queued register updates to the DDS. This ensures every
 * regiester is just written once
 */
typedef command command_spi_write;

/**
 * extra commands to set amplitude, phase and frequency. These are in most
 * ways identically to the regular register commands, but in case RAM is
 * active (more precise: command_state.single_tone_in_profile_regs is
 * false) they need to go in a different destination
 */
typedef struct
{
  command_type type;
  uint32_t value;
} command_frequency;

typedef struct
{
  command_type type;
  uint16_t value;
} command_amplitude;

typedef struct
{
  command_type type;
  uint16_t value;
} command_phase;

/**
 * start sending data to the DDS through the parallel port. All
 * configurations must be done before this command is issued. Usualy it
 * should be queued behind a trigger command.
 */
typedef struct
{
  command_type type;
  uint16_t* data;
  size_t length;
  size_t repeats;
} ALIGNED command_parallel;

/**
 * set the parallel update frequency. Timing for parallel is done by the
 * microprocessor.
 */
typedef struct
{
  command_type type;
  float frequency;
} ALIGNED command_parallel_frequency;

/**
 * Activate the RAM processing. Activating the RAM changes the meaning of
 * the profile registers which requires some extra handling which is done
 * by this command.
 */
typedef struct
{
  command_type type;
  uint8_t enable;
} ALIGNED command_ram_state;

/**
 * set the RAM target to the given value. We need a special command for
 * this because we might need to enable OSK mode to have the correct
 * amplitude settings.
 */
typedef struct
{
  command_type type;
  ad9910_ram_destination dest;
} ALIGNED command_ram_destination;

/**
 * Program the RAM settings into the given profile register.
 *
 * NOTE: The profile registers are ether used to control the RAM (when RAM
 * mode is enabled) or to set the output parameters (when RAM is
 * disabled). Writing RAM values without enabling RAM will result in
 * unwanted output settings.
 */
typedef struct
{
  command_type type;
  uint16_t start;
  uint16_t stop;
  uint16_t rate;
  uint8_t no_dwell;
  uint8_t zero_crossing;
  uint8_t profile;
} ALIGNED command_ram_program;

/**
 * change the current ram control mode. An extra command is necessary as
 * there is one register with value defined in enum ad9910_ram_control.
 * It controls however two independent functions, direction and mode which
 * can be set independently. We need this command to make sure that we
 * don't overwrite other changes.
 */
typedef struct
{
  command_type type;
  ad9910_ram_direction direction;
  ad9910_ram_mode mode;
} ALIGNED command_ram_control;

/**
 * An extra command for ramp state changes and destination changes. This
 * is necessary because we might need to change the handling of the
 * amplitude parameter if the RAM mode is enabled as well
 */
typedef struct
{
  command_type type;
  uint8_t value;
} ALIGNED command_ramp_state;
typedef struct
{
  command_type type;
  ad9910_ramp_destination value;
} ALIGNED command_ramp_destination;

/**
 * set the ramp direction to the specified direction by toggling to low,
 * high and back to low if necessary. This appears to be necessary to
 * start sequences. By the DDS documentation it should immediately start
 * the output if the pin is high, but it does not do that. Instead this
 * toggle is required.
 */
typedef struct
{
  command_type type;
  uint8_t value;
} ALIGNED command_ramp_direction_toggle;

#define COMMAND_QUEUE_DEF(cmd) int commands_queue_##cmd(const command_##cmd*);
COMMANDS_LIST(COMMAND_QUEUE_DEF)
#undef COMMAND_QUEUE_DEF

void commands_clear(void);
void commands_repeat(uint32_t);
uint32_t get_commands_repeat(void);
void commands_execute(void);

size_t commands_execute_command(const command*);

#define EXECUTE_COMMAND_DEF(cmd)                                               \
  size_t commands_execute_##cmd(const command_##cmd*);
COMMANDS_LIST(EXECUTE_COMMAND_DEF)
#undef EXECUTE_COMMAND_DEF

void startup_command_clear(void);
void startup_command_execute(void);
void startup_command_save(void);

#endif /* _COMMANDS_H */
