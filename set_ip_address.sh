#!/bin/bash

DEFAULT_ADDR=172.31.10.50
PORT=5024
ADDR=$1
NETMASK=255.255.255.0
GATEWAY=${ADDR%.*}.1


usage() {
    echo "$0 <ADDRESS>: configure the IP address of a DDS"
    echo
    echo "  this script connects to a DDS on the default address $DEFAULT_ADDR"
    echo "  and writes the new address configuration"
}

if [[ -z $ADDR ]] ; then
	usage
	exit 1
fi

echo "Writing configuration"
echo "Address:  $ADDR"
echo "Submask:  $NETMASK"
echo "Gateway:  $GATEWAY"

echo -en ":SYST:NET:ADDRess '$ADDR';SUBmask '$NETMASK';GATEway '$GATEWAY'\r\n" \
	| nc --idle-timeout=1 $DEFAULT_ADDR $PORT

if [[ $? == 0 ]] ; then
    echo "Success! Restart the DDS to activate the new config"
fi
