#include "commands.h"

#include "ad9910.h"
#include "crc.h"
#include "eeprom.h"
#include "gpio.h"
#include "timing.h"

#include <string.h>

uint8_t command_execute_flag = 0;

#define STARTUP_EEPROM eeprom_block0

struct commands_queue
{
  void* const begin;
  void* end; /* ptr behind the last used byte */
  uint32_t repeat;
};

static ALIGNED char commands_buf[COMMAND_QUEUE_LENGTH];

static struct commands_queue commands = {
  .begin = commands_buf,
  .end = commands_buf,
  .repeat = 0,
};

/**
 * state struct which is used to keep track of some states which are
 * dificult to track otherwise.
 */
static struct
{
  ad9910_ram_control ram_control;
  /* if this is true the single tone parameters are stored in the profile
   * regs. If it is false they are stored in the FTW, POW and ASF
   * registers. This is necessary for RAM operation */
  uint8_t single_tone_in_profile_regs;
} command_state = {
  .ram_control = ad9910_ram_ctl_ramp_up,
  .single_tone_in_profile_regs = 1,
};

static uint32_t update_registers = 0;

static int commands_queue_with_spi_write(command_type, const command*, size_t);

static void execute_commands(struct commands_queue*);
static int commands_queue(command_type, const void*, size_t);
static size_t get_command_length(const command*);

static void set_register(ad9910_register_bit, uint64_t);

/* helper functions to find elements in the list of commands */
static command* next_command(command*);
static command* find_command(command_type, command* begin, command* end);
static command* start_of_current_block(void);

#define DEFINE_COMMANDS_QUEUE(cmd)                                             \
  int commands_queue_##cmd(const command_##cmd* command)                       \
  {                                                                            \
    return commands_queue(command_type_##cmd, command, sizeof(command_##cmd)); \
  }

#define DEFINE_COMMANDS_QUEUE_WITH_SPI_WRITE(type)                             \
  int commands_queue_##type(const command_##type* cmd)                         \
  {                                                                            \
    return commands_queue_with_spi_write(                                      \
      command_type_##type, (const command*)cmd, sizeof(command_##type));       \
  }

DEFINE_COMMANDS_QUEUE_WITH_SPI_WRITE(register)
DEFINE_COMMANDS_QUEUE(pin)
DEFINE_COMMANDS_QUEUE(trigger)
DEFINE_COMMANDS_QUEUE(update)
DEFINE_COMMANDS_QUEUE(wait)
DEFINE_COMMANDS_QUEUE_WITH_SPI_WRITE(frequency)
DEFINE_COMMANDS_QUEUE_WITH_SPI_WRITE(amplitude)
DEFINE_COMMANDS_QUEUE_WITH_SPI_WRITE(phase)
DEFINE_COMMANDS_QUEUE(parallel)
DEFINE_COMMANDS_QUEUE(parallel_frequency)
DEFINE_COMMANDS_QUEUE_WITH_SPI_WRITE(ram_state)
DEFINE_COMMANDS_QUEUE_WITH_SPI_WRITE(ram_destination)
DEFINE_COMMANDS_QUEUE_WITH_SPI_WRITE(ram_program)
DEFINE_COMMANDS_QUEUE_WITH_SPI_WRITE(ramp_state)
DEFINE_COMMANDS_QUEUE_WITH_SPI_WRITE(ramp_destination)
DEFINE_COMMANDS_QUEUE(ramp_direction_toggle)

/**
 * Queue the command and append a spi write command. If there is another
 * spi write command in the current block it gets removed as one spi write
 * per block is enough.
 */
static int
commands_queue_with_spi_write(command_type type, const command* cmd,
                              size_t size)
{
  /* we do a search for an spi_write in the current block. If there is one
   * we remove it and replace it with ours as there is no need for
   * multiple spi_write per block */
  command* block_start = start_of_current_block();
  command* write =
    find_command(command_type_spi_write, block_start, commands.end);

  if (write != commands.end) {
    /* copy all following command over the write and decrement end ptr */
    const size_t size = sizeof(command_spi_write);
    memmove(write, (void*)write + size, commands.end - (void*)write - size);
    commands.end -= size;
  }

  size_t ret = commands_queue(type, cmd, size);
  ret += commands_queue(command_type_spi_write, cmd, sizeof(command_spi_write));

  return ret;
}

int
commands_queue_ram_control(const command_ram_control* cmd)
{
  /* if there is already a ram control since the last update we just want
   * to modify that one (in case direction and mode have been changed).
   * Otherwise we insert a ram control which needs to
   */
  command* block_start = start_of_current_block();
  command* find =
    find_command(command_type_ram_control, block_start, commands.end);

  if (find == commands.end) {
    /* no previous ram_control command */
    return commands_queue_with_spi_write(command_type_ram_control, (void*)cmd,
                                         sizeof(command_ram_control));
  } else {
    command_ram_control* old_cmd = (command_ram_control*)find;
    if (cmd->direction != ad9910_ram_direction_none) {
      old_cmd->direction = cmd->direction;
    }
    if (cmd->mode != ad9910_ram_mode_none) {
      old_cmd->mode = cmd->mode;
    }

    return 0;
  }
}

static int
commands_queue(command_type type, const void* data, size_t len)
{
  /* check if enough memory is left in the queue */
  if (commands.end - (void*)commands.begin + len > COMMAND_QUEUE_LENGTH) {
    return 1;
  }

  const command* cmd = data;

  command* header = commands.end;
  memcpy(commands.end, cmd, len);
  /* make sure the type is correct */
  header->type = type;
  commands.end += len;

  return 0;
}

/**
 * This function is used to disable or enable the OSK mode where
 * necessary. The OSK mode is needed when the RAM is enabled. In this case
 * the amplitude should be read from the ASF register unless some other
 * unit provides it. To enable reading the parameter from ASF we need to
 * enable OSK manual mode. We need to disable it if someone else should
 * provide the amplitude because OSK has highest priority.
 */
static void
set_osk_state(int enable)
{
  /* we only do something if the commands are not in the single tone
   * register. Otherwise we don't need the OSK mode */
  if (command_state.single_tone_in_profile_regs) {
    return;
  }

  /* disabling is easy, we just assume the caller is going to control the
   * amplitude and disable OSK */
  if (!enable) {
    set_register(ad9910_osk_enable, 0);
  } else {
    /* if we should enable it we first make sure that noone else is
     * providing the amplitude parameter */
    ad9910_ram_destination ram =
      ad9910_get_value(ad9910_ram_playback_destination);
    if (ram == ad9910_ram_dest_amplitude || ram == ad9910_ram_dest_polar ||
        (ad9910_get_value(ad9910_digital_ramp_enable) &&
         ad9910_get_value(ad9910_digital_ramp_destination) ==
           ad9910_ramp_dest_amplitude)) {
      return;
    }

    set_register(ad9910_manual_osk_external_control, 1);
    set_register(ad9910_osk_enable, 1);
    set_register(ad9910_select_auto_osk, 0);
  }
}

void
commands_clear()
{
  commands.end = commands.begin;
}

void
commands_repeat(uint32_t count)
{
  commands.repeat = count;
}

uint32_t
get_commands_repeat()
{
  return commands.repeat;
}

void
commands_execute()
{
  /* check if we lost the PLL to any reason, restart the DDS */
  if (gpio_get_input(PLL_LOCK) == 0) {
    ad9910_start();
  }

  execute_commands(&commands);
}

static void
execute_commands(struct commands_queue* cmds)
{
  uint32_t i = 0;

  gpio_set_high(LED_FRONT);

  do { /* repeat loop */
    void* cur = cmds->begin;

    while (cur < cmds->end) {
      cur += commands_execute_command(cur);
    }
  } while (++i < cmds->repeat);

  gpio_set_low(LED_FRONT);
}

#define commands_execute_SWITCH_DEF(input)                                     \
  case command_type_##input:                                                   \
    commands_execute_##input((const command_##input*)(cmd));                   \
    return sizeof(command_##input);

size_t
commands_execute_command(const command* cmd)
{
  switch (cmd->type) {
    COMMANDS_LIST(commands_execute_SWITCH_DEF)
  }

  return sizeof(command);
}

/* this should never be called and is here for completenes */
size_t
commands_execute_end(const command_end* cmd)
{
  return sizeof(command_end);
}

size_t
commands_execute_register(const command_register* cmd)
{
  ad9910_set_value(*cmd->reg, cmd->value);

  /* mark register for spi update */
  update_registers |= (1 << cmd->reg->reg->address);

  return sizeof(command_register);
}

size_t
commands_execute_spi_write(const command_spi_write* cmd)
{
  ad9910_update_multiple_regs(update_registers);

  update_registers = 0;

  return 0;
}

size_t
commands_execute_pin(const command_pin* cmd)
{
  gpio_set(cmd->pin, cmd->value);

  return sizeof(command_pin);
}

size_t
commands_execute_trigger(const command_trigger* cmd)
{
  /* TODO generalize for other trigger pins */
  gpio_set_pin_mode_input(IO_UPDATE);
  while (gpio_get_input(IO_UPDATE) == 0) {
  };
  while (gpio_get_input(IO_UPDATE) == 1) {
  };
  gpio_set_pin_mode_output(IO_UPDATE);

  return 0;
}

size_t
commands_execute_wait(const command_wait* cmd)
{
  delay(cmd->delay);

  return sizeof(command_wait);
}

size_t
commands_execute_update(const command_update* cmd)
{
  ad9910_io_update();

  return 0;
}

size_t
commands_execute_frequency(const command_frequency* cmd)
{
  if (command_state.single_tone_in_profile_regs) {
    set_register(ad9910_profile_frequency, cmd->value);
  } else {
    set_register(ad9910_ftw, cmd->value);
  }

  return sizeof(command_frequency);
}

size_t
commands_execute_amplitude(const command_amplitude* cmd)
{
  if (command_state.single_tone_in_profile_regs) {
    set_register(ad9910_profile_amplitude, cmd->value);
  } else {
    set_register(ad9910_amplitude_scale_factor, cmd->value);
  }

  return sizeof(command_amplitude);
}

size_t
commands_execute_phase(const command_phase* cmd)
{
  if (command_state.single_tone_in_profile_regs) {
    set_register(ad9910_profile_phase, cmd->value);
  } else {
    set_register(ad9910_pow, cmd->value);
  }

  return sizeof(command_phase);
}

size_t
commands_execute_parallel(const command_parallel* cmd)
{
  if (gpio_get_output(PARALLEL_F0) == gpio_get_output(PARALLEL_F1)) {
    set_osk_state(0);
  };

  ad9910_execute_parallel(cmd->data, cmd->length, cmd->repeats);

  if (gpio_get_output(PARALLEL_F0) == gpio_get_output(PARALLEL_F1)) {
    set_osk_state(1);
  };

  return sizeof(command_parallel);
}

size_t
commands_execute_parallel_frequency(const command_parallel_frequency* cmd)
{
  ad9910_set_parallel_frequency(cmd->frequency);

  return sizeof(command_parallel_frequency);
}

size_t
commands_execute_ram_state(const command_ram_state* cmd)
{
  if (ad9910_get_value(ad9910_ram_enable) == cmd->enable) {
    return sizeof(command_ram_state);
  }

  set_register(ad9910_ram_enable, cmd->enable);

  /* make sure the single tone values are in the correct registers */
  if (cmd->enable && command_state.single_tone_in_profile_regs) {
    set_register(ad9910_ftw, ad9910_get_value(ad9910_profile_frequency));
    set_register(ad9910_pow, ad9910_get_value(ad9910_profile_phase));
    set_register(ad9910_amplitude_scale_factor,
                 ad9910_get_value(ad9910_profile_amplitude));
    command_state.single_tone_in_profile_regs = 0;
  } else if (!cmd->enable && !command_state.single_tone_in_profile_regs) {
    set_register(ad9910_profile_frequency, ad9910_get_value(ad9910_ftw));
    set_register(ad9910_profile_phase, ad9910_get_value(ad9910_pow));
    set_register(ad9910_profile_amplitude,
                 ad9910_get_value(ad9910_amplitude_scale_factor));
    command_state.single_tone_in_profile_regs = 1;
  }

  set_osk_state(cmd->enable);

  return sizeof(command_ram_state);
}

size_t
commands_execute_ram_destination(const command_ram_destination* cmd)
{
  set_register(ad9910_ram_playback_destination, cmd->dest);

  set_osk_state(1);

  return sizeof(command_ram_destination);
}

size_t
commands_execute_ram_program(const command_ram_program* cmd)
{
  /* clear register first. This should allow the compiler to optimize most
   * of this away */
  ad9910_register* reg = ad9910_get_profile_reg(cmd->profile);
  reg->value = 0;

  ad9910_set_profile_value(cmd->profile, ad9910_profile_address_step_rate,
                           cmd->rate);
  ad9910_set_profile_value(cmd->profile, ad9910_profile_waveform_end_address,
                           cmd->stop);
  ad9910_set_profile_value(cmd->profile, ad9910_profile_waveform_start_address,
                           cmd->start);
  ad9910_set_profile_value(cmd->profile, ad9910_profile_no_dwell_high,
                           cmd->no_dwell);
  ad9910_set_profile_value(cmd->profile, ad9910_profile_zero_crossing,
                           cmd->zero_crossing);
  ad9910_set_profile_value(cmd->profile, ad9910_profile_ram_mode_control,
                           command_state.ram_control);

  /* mark register for spi update */
  update_registers |= (1 << reg->address);

  return sizeof(command_ram_program);
}

size_t
commands_execute_ram_control(const command_ram_control* cmd)
{
  int dir = cmd->direction != ad9910_ram_direction_none
              ? cmd->direction
              : ad9910_ram_get_direction(
                  ad9910_get_value(ad9910_profile_ram_mode_control));
  int mode =
    cmd->mode != ad9910_ram_mode_none
      ? cmd->mode
      : ad9910_ram_get_mode(ad9910_get_value(ad9910_profile_ram_mode_control));

  command_state.ram_control = ad9910_ram_get_control(dir, mode);

  /* we only set the mode directly if we are in RAM mode. Otherwise we
   * would change bits in the frequency register */
  if (ad9910_get_value(ad9910_ram_enable)) {
    const command_register ncmd = {
      .reg = &ad9910_profile_ram_mode_control,
      .value = command_state.ram_control,
    };

    commands_execute_register(&ncmd);
  };

  return sizeof(command_ram_control);
}

size_t
commands_execute_ramp_state(const command_ramp_state* cmd)
{
  set_register(ad9910_digital_ramp_enable, cmd->value);

  if (cmd->value &&
      ad9910_get_value(ad9910_digital_ramp_destination) ==
        ad9910_ramp_dest_amplitude) {
    set_osk_state(0);
  } else {
    set_osk_state(1);
  }

  return sizeof(command_ramp_state);
}

size_t
commands_execute_ramp_destination(const command_ramp_destination* cmd)
{
  set_register(ad9910_digital_ramp_destination, cmd->value);

  if (cmd->value == ad9910_ramp_dest_amplitude) {
    set_osk_state(0);
  } else {
    set_osk_state(1);
  }

  return sizeof(command_ramp_destination);
}

size_t
commands_execute_ramp_direction_toggle(const command_ramp_direction_toggle* cmd)
{
  gpio_set_low(DRCTL);
  gpio_set_high(DRCTL);
  if (!cmd->value) {
    gpio_set_low(DRCTL);
  }

  return sizeof(command_ramp_direction_toggle);
}

void
startup_command_clear()
{
  eeprom_erase(STARTUP_EEPROM);
}

void
startup_command_execute()
{
  uint32_t* crc_saved = eeprom_get(STARTUP_EEPROM, 0);
  crc_init();
  uint32_t crc_calc = crc(eeprom_get(STARTUP_EEPROM, sizeof(crc_calc)),
                          (eeprom_get_size(STARTUP_EEPROM) - sizeof(crc_calc)) /
                            sizeof(uint32_t));

  /* if the crc check fails we abort */
  if (crc_calc != *crc_saved) {
    return;
  }

  uint32_t* len = eeprom_get(STARTUP_EEPROM, sizeof(crc_saved));

  struct commands_queue commands = {
    .begin = len + 1, .end = ((char*)(len + 1)) + *len, .repeat = 0,
  };

  execute_commands(&commands);
}

void
startup_command_save()
{
  startup_command_clear();

  uint32_t len = commands.end - commands.begin;

  uint32_t crcsum;

  /* write length of the command sequence */
  eeprom_write(STARTUP_EEPROM, sizeof(crcsum), &len, sizeof(len));
  /* save command sequence behind it */
  eeprom_write(STARTUP_EEPROM, sizeof(crcsum) + sizeof(len), commands.begin,
               len);

  crc_init();
  crcsum =
    crc(eeprom_get(STARTUP_EEPROM, sizeof(crcsum)),
        (eeprom_get_size(STARTUP_EEPROM) - sizeof(crcsum)) / sizeof(uint32_t));

  /* save crc at the begining */
  eeprom_write(STARTUP_EEPROM, 0, &crcsum, sizeof(crcsum));
}

#define GET_COMMAND_LENGTH_DEF(cmd)                                            \
  case command_type_##cmd:                                                     \
    return sizeof(command_##cmd);

static size_t
get_command_length(const command* cmd)
{
  switch (cmd->type) {
    COMMANDS_LIST(GET_COMMAND_LENGTH_DEF)
  }

  return sizeof(command);
}

static void
set_register(ad9910_register_bit reg, uint64_t value)
{
  command_register cmd = {
    .reg = &reg, .value = value,
  };
  commands_execute_register(&cmd);
}

static command*
next_command(command* cmd)
{
  size_t len = get_command_length(cmd);

  return ((void*)cmd) + len;
}

/**
 * return the first occurence of the given command_type in the given
 * range. If no matching command is found end is returened
 */
static command*
find_command(command_type type, command* begin, command* end)
{
  for (command* cur = begin; cur < end; cur = next_command(cur)) {
    if (cur->type == type) {
      return cur;
    }
  }

  return end;
}

/**
 * find the first command in the current command block. Blocks are
 * separated by trigger and update commands
 */
static command*
start_of_current_block()
{
  command* found = NULL;
  for (command* cur = commands.begin; cur < (command*)commands.end;
       cur = next_command(cur)) {
    if (cur->type == command_type_trigger || cur->type == command_type_update) {
      found = cur;
    }
  }

  if (found == NULL) {
    /* if we haven't found anythingeverything is just one block */
    return commands.begin;
  } else {
    return next_command(found);
  }
}
