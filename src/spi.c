#include "spi.h"

#include <stm32f4xx_dma.h>
#include <stm32f4xx_gpio.h>
#include <stm32f4xx_rcc.h>
#include <stm32f4xx_spi.h>

/**
 * SPI communication with the AD9910. Uses the TM library to make
 * programming a bit simpler. For documentation on that see here:
 * http://stm32f4-discovery.com/2014/04/library-05-spi-for-stm32f4xx/
 *
 * See the AD9910 data sheet starting on page 48 for information on the
 * serial programming interface of the DDS chip.
 */

static uint32_t rx_dummy_buffer;

static int dma_enabled = 0;

void
spi_init_slow()
{
  spi_init(SPI_BaudRatePrescaler_256);
}

void
spi_init_fast()
{
  spi_init(SPI_BaudRatePrescaler_2);
}

/**
 * this functions sets up all the spi communication. To only usage of SPI
 * is currently the communication between AD9910 and the STM32F4
 */
void
spi_init(uint16_t prescaler)
{
  SPI_InitTypeDef spi_init;

  SPI_StructInit(&spi_init);

  /* enable SPI clock */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE);

  /* init pins */
  GPIO_InitTypeDef GPIO_InitStructure;
  GPIO_InitStructure.GPIO_Speed = GPIO_High_Speed;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5;
  GPIO_Init(GPIOB, &GPIO_InitStructure);
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource3, GPIO_AF_SPI1);
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource4, GPIO_AF_SPI1);
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource5, GPIO_AF_SPI1);

  /* set options */
  spi_init.SPI_DataSize = SPI_DataSize_8b;
  spi_init.SPI_BaudRatePrescaler = prescaler;
  spi_init.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
  spi_init.SPI_FirstBit = SPI_FirstBit_MSB;
  spi_init.SPI_Mode = SPI_Mode_Master;
  spi_init.SPI_NSS = SPI_NSS_Soft;
  /* SPI mode */
  spi_init.SPI_CPOL = SPI_CPOL_Low;
  spi_init.SPI_CPHA = SPI_CPHA_1Edge;

  /* disable first */
  SPI_Cmd(SPI1, DISABLE);

  SPI_Init(SPI1, &spi_init);

  /* enable SPI */
  SPI_Cmd(SPI1, ENABLE);
}

void
spi_init_dma()
{
  DMA_InitTypeDef tx_struct;
  DMA_InitTypeDef rx_struct;

  /* enable DMA2 clock */
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2, ENABLE);

  /* we don't use the struct here, we just prepare all the settings */
  tx_struct.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
  tx_struct.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
  tx_struct.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
  tx_struct.DMA_Mode = DMA_Mode_Normal;
  tx_struct.DMA_Priority = DMA_Priority_VeryHigh;
  tx_struct.DMA_FIFOMode = DMA_FIFOMode_Disable;
  tx_struct.DMA_FIFOThreshold = DMA_FIFOThreshold_1QuarterFull;
  tx_struct.DMA_MemoryBurst = DMA_MemoryBurst_Single;
  tx_struct.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;

  tx_struct.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
  tx_struct.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;

  tx_struct.DMA_Channel = SPI1_DMA_TX_CHANNEL;
  tx_struct.DMA_DIR = DMA_DIR_MemoryToPeripheral;

  tx_struct.DMA_PeripheralBaseAddr = (uint32_t)&SPI1->DR;
  tx_struct.DMA_MemoryInc = DMA_MemoryInc_Enable;

  /* rx buffer */
  rx_struct.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
  rx_struct.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
  rx_struct.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
  rx_struct.DMA_Mode = DMA_Mode_Normal;
  rx_struct.DMA_Priority = DMA_Priority_VeryHigh;
  rx_struct.DMA_FIFOMode = DMA_FIFOMode_Disable;
  rx_struct.DMA_FIFOThreshold = DMA_FIFOThreshold_1QuarterFull;
  rx_struct.DMA_MemoryBurst = DMA_MemoryBurst_Single;
  rx_struct.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
  rx_struct.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
  rx_struct.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;

  rx_struct.DMA_Channel = SPI1_DMA_RX_CHANNEL;
  rx_struct.DMA_DIR = DMA_DIR_PeripheralToMemory;

  rx_struct.DMA_PeripheralBaseAddr = (uint32_t)&SPI1->DR;

  /* change this if you wan't to receive something using DMA */
  rx_struct.DMA_MemoryInc = DMA_MemoryInc_Disable;
  rx_struct.DMA_Memory0BaseAddr = (uint32_t)&rx_dummy_buffer;

  DMA_Init(SPI1_DMA_TX_STREAM, &tx_struct);
  DMA_Init(SPI1_DMA_RX_STREAM, &rx_struct);

  dma_enabled = 1;
}

void
spi_deinit()
{
  spi_wait();

  if (dma_enabled) {
    DMA_DeInit(SPI1_DMA_TX_STREAM);
    DMA_DeInit(SPI1_DMA_RX_STREAM);
    dma_enabled = 0;
  }
  SPI_DeInit(SPI1);
}

void
spi_write_multi(uint8_t* data, uint32_t length)
{
  if (length == 0) {
    return;
  }

  spi_wait();

  if (dma_enabled) {
    /* first disable DMA controller */
    /* inlined DMA_Cmd(SPI1_DMA_TX_STREAM, DISABLE); */
    SPI1_DMA_RX_STREAM->CR &= ~(uint32_t)DMA_SxCR_EN;
    SPI1_DMA_TX_STREAM->CR &= ~(uint32_t)DMA_SxCR_EN;

    /* update data length and target */
    SPI1_DMA_TX_STREAM->M0AR = (uint32_t)data;
    SPI1_DMA_TX_STREAM->NDTR = (uint16_t)length;
    SPI1_DMA_RX_STREAM->NDTR = (uint16_t)length;

    /* clear transfer complete flags */
    /* inlined DMA_ClearFlag(SPI1_DMA_RX_STREAM, DMA_FLAG_TCIF2); */
    DMA2->LIFCR = (uint32_t)(DMA_FLAG_TCIF2 & (uint32_t)0x0F7D0F7D);
    /* inlined DMA_ClearFlag(SPI1_DMA_TX_STREAM, DMA_FLAG_TCIF3); */
    DMA2->LIFCR = (uint32_t)(DMA_FLAG_TCIF3 & (uint32_t)0x0F7D0F7D);

    /* enable DMA controller */
    /* inlined DMA_Cmd(SPI1_DMA_TX_STREAM, ENABLE); */
    SPI1_DMA_RX_STREAM->CR |= (uint32_t)DMA_SxCR_EN;
    SPI1_DMA_TX_STREAM->CR |= (uint32_t)DMA_SxCR_EN;

    /* activate SPI transfer */
    SPI1->CR2 |= SPI_CR2_RXDMAEN | SPI_CR2_TXDMAEN;
  } else {
    for (uint32_t i = 0; i < length; ++i) {
      spi_send_single(*data++);
    }
  }
}
